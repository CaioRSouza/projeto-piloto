'use strict';
var server = require('../server');
var ds = server.dataSources.db;
const models = ['Pessoa'];

ds.automigrate(models, function(err, result) {
  ds.disconnect();
});
