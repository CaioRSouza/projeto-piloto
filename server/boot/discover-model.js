'use strict';

module.exports = {
  pessoa: function(app, callback) {
    const dataSource = app.dataSources.db;
    dataSource.discoverAndBuildModels(
      'Pessoa',
      {
        relations: true,
        idInjection: false,
      },
        function(err, models) {
          if (err) return callback(err);
          for (const modelName in models) {
            app.model(models[modelName], {dataSource: dataSource});
          }
          callback();
        });
  },
};
