'use strict';

const loopback = require('loopback');
const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdirp = promisify(require('mkdirp'));
const DATASOURCE_NAME = 'db';
const dataSourceConfig = require('./server/datasources.json');
const db = new loopback.DataSource(dataSourceConfig[DATASOURCE_NAME]);
discover().then(
    success => process.exit(),
    error => { console.error('UNHANDLED ERROR:\n', error); process.exit(1); }
);
async function discover() {
    const options = { relations: true };
    const pessoaSchemas = await db.discoverSchemas('Pessoa', options);

    await mkdirp('common/models');
    await writeFile(
        'common/models/pessoa.json',
        JSON.stringify(pessoaSchemas['cryptojud.Pessoa'], null, 2)
    );

    const configJson = await readFile('server/model-config.json', 'utf-8');
    console.log('MODEL CONFIG', configJson);
    const config = JSON.parse(configJson);
    config.Pessoa = { dataSource: DATASOURCE_NAME, public: true };

    await writeFile(
        'server/model-config.json',
        JSON.stringify(config, null, 2)
    );
}